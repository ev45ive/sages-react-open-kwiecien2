## GIT 
git clone https://ev45ive@bitbucket.org/ev45ive/sages-react-open-kwiecien2.git
code sages-react-open-kwiecien2

git stash -u
git pull
npm ci 
npm start

https://semver.npmjs.com/

## Instalacje
node -v
v16.13.1

npm -v
8.1.2

git --version
git version 2.31.1.windows.1

vs code -> HELP -> About
1.66.1

chrome://version/
Google Chrome	100.0.4896.60


## Create React APP
https://create-react-app.dev/

npx create-react-app --help
npx create-react-app --version
4.0.3

npx create-react-app . --template typescript
npx create-react-app@latest . --template typescript

https://github.com/gsoft-inc/craco 

## GIT
https://git-scm.com/download/win

"git.path": "C:\\Program Files\\Git\\bin"
C:\Users\ev45i\.gitconfig

## Bootstrap CSS
https://getbootstrap.com/docs/5.1/getting-started/introduction/

https://marketplace.visualstudio.com/items?itemName=thekalinga.bootstrap4-vscode

npm install bootstrap
https://react-bootstrap.github.io/components/alerts

## VS Extensions

https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets
https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode
https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype

## Emmet
https://docs.emmet.io/cheat-sheet/

## Playlists module

<!-- 
src/components/PlaylistList
src/pages/PlaylistsView 
-->
mkdir -p src/playlists/components/
mkdir -p src/playlists/containers/
mkdir -p src/core/components/

touch src/core/components/Button.tsx

touch src/playlists/components/PlaylistList.tsx
touch src/playlists/components/PlaylistDetails.tsx
touch src/playlists/components/PlaylistEditor.tsx

touch src/playlists/containers/PlaylistsView.tsx

## Music module 

touch src/core/services/MusicAPI.tsx

mkdir -p src/music/components/
mkdir -p src/music/containers/

touch src/music/containers/AlbumSearchView.tsx

touch src/music/components/SearchForm.tsx
touch src/music/components/SearchResults.tsx
touch src/music/components/AlbumCard.tsx

## Axios + OAuth

npm i axios

https://www.npmjs.com/package/react-oauth2-hook
npm i --legacy-peer-deps  react-oauth2-hook immutable prop-types react-storage-hook

holoyis165@bulkbye.com
placki 777 

## Fetcher Hooks
https://use-http.com/#/
https://react-query.tanstack.com/
https://swr.vercel.app/

## Form hooks
https://formik.org/docs/api/useFormik
https://react-hook-form.com/

## React Router v6 DOM
https://reactrouter.com/docs/en/v6/getting-started/tutorial


## Redux
https://redux.js.org/
https://react-redux.js.org/

https://redux-toolkit.js.org/tutorials/typescript
https://redux.js.org/style-guide/style-guide

https://medium.com/@dan_abramov/you-might-not-need-redux-be46360cf367
https://ngrx.io/guide/store/why


## Testing Library
https://testing-library.com/docs/queries/about