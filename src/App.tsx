import axios from "axios";
import React from "react";
import { OAuthCallback } from "react-oauth2-hook";
import { Navigate, Route, Routes } from "react-router-dom";
import Navbar from "./core/components/Navbar";
import AlbumDetailsView from "./music/containers/AlbumDetailsView";
import AlbumSearchView from "./music/containers/AlbumSearchView";
import PlaylistsReducerView from "./playlists/containers/PlaylistsReducerView";
import PlaylistsView from "./playlists/containers/PlaylistsView";

function App() {
  if (window.location.href.includes("/callback")) return <OAuthCallback />;

  return (
    <div>
      <Navbar />

      <div className="container">
        <div className="row">
          <div className="col">
            <Routes>
              <Route path="" element={<Navigate to={"music/search"} />} />
              <Route path="music/search" element={<AlbumSearchView />} />
              <Route path="music/albums/:albumId" element={<AlbumDetailsView />} />
              {/* <Route path="playlists" element={<PlaylistsView />} /> */}
              <Route path="playlists" element={<PlaylistsReducerView />} />
              <Route
                path="*"
                element={
                  <div className="my-auto p-5 text-center">
                    <h1 className="display-1">404</h1>
                    <p>Page not Found</p>
                  </div>
                }
              />
            </Routes>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
