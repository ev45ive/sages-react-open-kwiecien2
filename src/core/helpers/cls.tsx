// https://www.npmjs.com/package/clsx
export const cls = (...classes: (string | boolean)[]): string => {
  return classes.filter((c) => typeof c === "string").join(" ");
};
