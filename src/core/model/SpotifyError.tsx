// axios.interceptors.request.use(config =>{// })
interface SpotifyError {
  error: {
    status: number;
    message: string;
  };
}
// https://github.com/colinhacks/zod#parse
export function isSpotifyError(data: any): data is SpotifyError {
  return "error" in data && "message" in data.error && "status" in data.error;
}
