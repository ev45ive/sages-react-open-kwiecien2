import axios from "axios";
import { Album, AlbumResponse, AlbumSearchResponse } from "../model/Search";

export const fetchAlbumById = async (id = "") => {
  const { data } = await axios.get<unknown>("albums/" + id, {});
  assertAlbumResponse(data);
  return data;
};

function assertAlbumResponse(data: any): asserts data is AlbumResponse {
  if (data.type !== "album") throw new Error("Unexpected server response");
}

export const fetchAlbumSearchResults = async (query = "") => {
  const { data } = await axios.get<unknown>("search", {
    params: {
      type: "album",
      query,
    },
  });
  assertAlbumSearchResponse(data);
  return data.albums.items;
};

function assertAlbumSearchResponse(
  data: any
): asserts data is AlbumSearchResponse {
  if (!Array.isArray(data.albums.items))
    throw new Error("Unexpected server response");
}

export const albumMocks: Album[] = [
  {
    id: "123",
    name: "Album 123",
    images: [
      {
        url: "https://www.placecage.com/c/100/100",
        height: 200,
        width: 200,
      },
    ],
  },
  {
    id: "234",
    name: "Album 234",
    images: [
      {
        url: "https://www.placecage.com/c/200/200",
        height: 200,
        width: 200,
      },
    ],
  },
  {
    id: "345",
    name: "Album 345",
    images: [
      {
        url: "https://www.placecage.com/c/300/300",
        height: 200,
        width: 200,
      },
    ],
  },
  {
    id: "456",
    name: "Album 456",
    images: [
      {
        url: "https://www.placecage.com/c/400/400",
        height: 200,
        width: 200,
      },
    ],
  },
];
