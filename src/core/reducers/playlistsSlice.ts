import { Playlist } from "../model/Playlist"
import { createSlice, PayloadAction, createAsyncThunk } from '@reduxjs/toolkit'
import { RootState } from "../store"
import { fetchPlaylistById, fetchPlaylists } from "../services/PlaylistsAPI"

// Define a type for the slice state
interface PlaylistsState {
  items: Playlist[],
  message?: string
}

// Define the initial state using that type
const initialState: PlaylistsState = {
  items: []
}

export const playlistsSlice = createSlice({
  name: 'playlists',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    // playlistsLoad(state, action: PayloadAction<{ data: Playlist[] }>) {
    //   // immer.js -> Immutable via Proxy
    //   state.items = action.payload.data
    // },
    // playlistLoad(state, action: PayloadAction<{ data: Playlist }>) {
    //   state.items = state.items.map(p => p.id === action.payload.data.id ? action.payload.data : p)
    // },
    playlistsRemove(state, action: PayloadAction<{ id: Playlist['id'] }>) {
      state.items = state.items.filter(p => p.id !== action.payload.id)
    },
    playlistsUpdate(state, action: PayloadAction<{ data: Playlist }>) {
      const draft = action.payload.data
      const index = state.items.findIndex(p => p.id === draft.id)
      state.items[index] = draft
    },
    playlistsAdd(state, action: PayloadAction<{ data: Playlist }>) {
      const draft = action.payload.data
      state.items.push(draft)
    },
  },
  extraReducers: (builder) => {
    // Add reducers for additional action types here, and handle loading state as needed
    // builder.addCase(fetchPlaylistsAction.pending, (state, action) => {
    // builder.addCase(fetchPlaylistsAction.rejected, (state, action) => {
    builder.addCase(fetchPlaylistsAction.fulfilled, (state, action) => {
      // Add user to the state array
      state.items = action.payload
    })
    builder.addCase(fetchPlaylistByIdAction.fulfilled, (state, action) => {
      // Add user to the state array
      state.items = state.items.map(p => p.id === action.payload.id ? action.payload : p)
    })
    builder.addMatcher(action => action.type.includes('rejected'), (state, action) => {
      state.message = action.error.message
    })
  },
})

export const { playlistsAdd, playlistsRemove, playlistsUpdate } = playlistsSlice.actions

// Other code such as selectors can use the imported `RootState` type
export const selectPlaylists = (state: RootState) => state.playlists.items
export const selectPlaylistById = (id?: Playlist['id'] | null) => (state: RootState) => state.playlists.items.find(p => p.id === id)

export default playlistsSlice.reducer


export const fetchPlaylistsAction = createAsyncThunk(
  'playlists/fetchPlaylists',
  async (thunkAPI) => {
    // thunkAPI.dispatch(...)
    // thunkAPI.dispatch(...)
    // thunkAPI.dispatch(...)
    const response = await fetchPlaylists()
    return response
  }
)

export const fetchPlaylistByIdAction = createAsyncThunk(
  'playlists/fetchPlaylistById',
  async (id: string, thunkAPI) => {
    const response = await fetchPlaylistById(id)
    return response
  }
)