import React, { useContext } from "react";
import { UserContext } from "../contexts/UserContext";

export const UserWidget = (props: {}) => {
  const context = useContext(UserContext);

  if (!context)
    throw new Error("Wrap <UserWidget> inside <UserContextProvider/>");

  const { user, login, logout } = context;

  return (
    <div>
      {user && (
        <span>
          Welcome {user.display_name} | <span onClick={logout}>Logout</span>
        </span>
      )}

      {!user && (
        <span>
          Welcome Guest | <span onClick={login}>Login</span>
        </span>
      )}
    </div>
  );
};
