import React, { useEffect, useState } from "react";
import { Album, AlbumResponse } from "../../core/model/Search";
import { fetchAlbumSearchResults } from "../../core/services/MusicAPI";
import SearchForm from "../components/SearchForm";
import SearchResults from "../components/SearchResults";
import { Loading } from "../../core/components/Loading";
import { useFetch } from "../../core/hooks/useFetch";
import { useLocation, useParams, useSearchParams } from "react-router-dom";
import { useQuery } from "react-query";

type Props = {};

const AlbumSearchView = (props: Props) => {
  const [searchParams, setSearchParams] = useSearchParams({ q: "" });
  const query = searchParams.get("q") || "";

  const {
    data: results,
    error,
    isLoading,
    refetch,
    remove,
  } = useQuery(
    "albumsSearch:" + query,
    async () => (query ? fetchAlbumSearchResults(query) : undefined),
    {}
  );

  const search = (query: string) => {
    setSearchParams(
      { q: query },
      {
        /* replace: true */
      }
    );
  };

  return (
    <div>
      <h3 className="display-3">Search</h3>

      <div className="row mb-3">
        <div className="col">
          <SearchForm onSearch={search} query={query} />
        </div>
      </div>
      <div className="row mb-3">
        <div className="col">
          {error instanceof Error && (
            <p className="alert alert-danger"> {error.message}</p>
          )}

          {query && <p>Searching for "{query}"</p>}

          {isLoading && <Loading />}

          {results && <SearchResults results={results} />}
        </div>
      </div>
    </div>
  );
};

export default AlbumSearchView;
