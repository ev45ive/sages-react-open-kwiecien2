// tsrafce
import React from "react";
import { Playlist } from "../../core/model/Playlist";

/* CSSinJS / Css modules */
import styles from "./PlaylistDetails.module.css";

type Props = {
  playlist?: Playlist;
  onEdit: () => void;
};

const PlaylistDetails = React.memo(({ playlist, onEdit }: Props) => {
  if (!playlist) {
    return <p className="alert alert-info">No playlist selected</p>;
  }

  return (
    <div>
      <dl title="Playlista 123" data-placki="123">
        <dt>Name</dt>
        <dd>{playlist.name}</dd>

        <dt>Public</dt>
        <dd
          className={styles.playlist_access}
          style={{
            color: playlist.public ? "red" : "green",
          }}
        >
          {playlist.public ? "Yes" : "No"}
        </dd>

        <dt>Description</dt>
        <dd>{playlist.description}</dd>
      </dl>

      <button onClick={onEdit} className="btn btn-info float-end mt-4">
        Edit
      </button>
    </div>
  );
});

export default PlaylistDetails;
