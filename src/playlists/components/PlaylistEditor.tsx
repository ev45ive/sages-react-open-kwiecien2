import React, {
  ChangeEvent,
  DragEvent,
  useEffect,
  useId,
  useRef,
  useState,
} from "react";
import { Playlist } from "../../core/model/Playlist";

type Props = {
  playlist?: Playlist;
  onCancel: () => void;
  onSave: (draft: Playlist) => void;
};

const EMPTY_PLAYLIST: Playlist = {
  id: "",
  name: "",
  description: "",
  public: false,
};

const PlaylistEditor = React.memo(
  ({ playlist = EMPTY_PLAYLIST, onCancel, onSave }: Props) => {
    const [playlistName, setPlaylistName] = useState(playlist.name);
    const [playlistPublic, setPlaylistPublic] = useState(playlist.public);
    const [playlistDescription, setPlaylistDescription] = useState(
      playlist.description
    );

    useEffect(() => {
      setPlaylistName(playlist.name);
      setPlaylistPublic(playlist.public);
      setPlaylistDescription(playlist.description);
    }, [playlist]);

    const nameInputRef = useRef<HTMLInputElement>(null);

    useEffect(() => {
      nameInputRef.current?.focus();
    }, []);

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
      setPlaylistName(event.currentTarget.value);
    };

    const handleSubmit = () => {
      onSave({
        ...playlist,
        name: playlistName,
        public: playlistPublic,
        description: playlistDescription,
      });
    };

    const id = useId();

    return (
      <div>
        <div className="form-group mb-3">
          <label htmlFor={id + "_name"}>Name</label>
          <input
            ref={nameInputRef}
            type="text"
            className="form-control"
            name={id + "_name"}
            id={id + "_name"}
            value={playlistName}
            onChange={handleChange}
            placeholder="Name"
          />
          <small className="form-text text-muted">
            {playlistName.length} / 100
          </small>
        </div>

        <div className="form-check mb-3">
          <label className="form-check-label">
            <input
              type="checkbox"
              className="form-check-input"
              name={id + "_public"}
              id={id + "_public"}
              checked={playlistPublic}
              onChange={(event) =>
                setPlaylistPublic(event.currentTarget.checked)
              }
            />
            Public
          </label>
        </div>

        <div className="form-group mb-3">
          <label htmlFor={id + "_description"}>Description</label>
          <textarea
            className="form-control"
            name={id + "_description"}
            id={id + "_description"}
            rows={3}
            value={playlistDescription}
            onChange={(e) => setPlaylistDescription(e.currentTarget.value)}
          ></textarea>
        </div>

        <button
          className="btn btn-success float-end mt-4"
          onClick={handleSubmit}
        >
          Save
        </button>
        <button className="btn btn-danger float-end mt-4" onClick={onCancel}>
          Cancel
        </button>

        {/* <pre>{JSON.stringify(playlist, null, 2)}</pre>
      <pre>
        {JSON.stringify(
          {
            name: playlistName,
            public: playlistPublic,
            description: playlistDescription,
          },
          null,
          2
        )}
      </pre> */}
      </div>
    );
  }
);

export default PlaylistEditor;
